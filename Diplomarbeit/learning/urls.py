from django.conf.urls import url
from . import views

urlpatterns = [
    # /learning/
    url(r'^$', views.index, name='index'),
    # /learning/monitoring
    url(r'^monitoring/$', views.monitoring, name='monitoring'),
    # /learning/figure_blue.png
    url(r'^figure_(?P<color>[\w.@+-]+).png$', views.figure, name='figure'),
    # /learning/main.png
    url(r'^main.png$', views.main, name='main'),
    # /learning/detail
    url(r'^detail/$', views.detail, name='detail'),
    # /learning/relevant_blue_1_123_456
    url(r'^relevant_(?P<color>[\w.@+-]+)_(?P<ts_id1>[0-9]+)_(?P<ts_id2>[0-9]+)_(?P<ts_id3>[0-9]+)/$', views.relevant,
        name='relevant'),
    # /learning/irrelevant_blue_1_123_456
    url(r'^irrelevant_(?P<color>[\w.@+-]+)_(?P<ts_id1>[0-9]+)_(?P<ts_id2>[0-9]+)_(?P<ts_id3>[0-9]+)/$', views.irrelevant
        , name='irrelevant'),
]
