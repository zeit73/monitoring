import numpy as np
import pandas as pd


class PyTimeBlue(object):

    def __init__(self, data, unit):
        self.relevance = 2  # 0 - not relevant // 1 - relevant
        self.min = np.min(data['values'])
        self.max = np.max(data['values'])
        self.mean = np.mean(data['values'])
        self.std = np.std(data['values'])
        self.span = abs((pd.to_datetime(data['time_stamp'].iloc[-1]) -
                        pd.to_datetime(data['time_stamp'].iloc[0])).total_seconds())  # alt.Stunden:.seconds//3600
        self.unit = unit                                                                    # .days --> .seconds nehmen

    def __str__(self):
        return ''

    def get_min(self):
        return self.min

    def get_max(self):
        return self.max

    def get_mean(self):
        return self.mean

    def get_std(self):
        return self.std

    def get_rel(self):
        return self.relevance

    def get_span(self):
        return self.span

    def get_rows(self):
        return self.rows

    def get_unit(self):
        return self.unit

    def get_val(self):
        return self.val_date

    def set_rel(self):
        self.relevance = 1

    def set_irrel(self):
        self.relevance = 0


class PyTimeGreen(object):

    def __init__(self, data, unit):
        self.relevance = 2  # 0 - not relevant // 1 - relevant
        self.min = np.min(data['values'])
        self.max = np.max(data['values'])
        self.start_second = int(pd.to_datetime(data['time_stamp'].iloc[0]).second)
        self.end_second = int(pd.to_datetime(data['time_stamp'].iloc[-1]).second)
        self.span = abs((pd.to_datetime(data['time_stamp'].iloc[-1]) -
                        pd.to_datetime(data['time_stamp'].iloc[0])).total_seconds())  # alt.Stunden:.seconds//3600
        self.unit = unit                                                                    # .days --> .seconds nehmen

    def __str__(self):
        return ''

    def get_min(self):
        return self.min

    def get_max(self):
        return self.max

    def get_start_second(self):
        return self.start_second

    def get_end_second(self):
        return self.end_second

    def get_rel(self):
        return self.relevance

    def get_span(self):
        return self.span

    def get_unit(self):
        return self.unit

    def set_rel(self):
        self.relevance = 1

    def set_irrel(self):
        self.relevance = 0


class PyTimeRed(object):

    def __init__(self, data, unit):
        self.relevance = 2  # 0 - not relevant // 1 - relevant
        self.mean = np.min(data['values'])
        self.std = np.std(data['values'])
        self.skew = data['values'].skew()
        self.kurt = data['values'].kurtosis()
        self.span = abs((pd.to_datetime(data['time_stamp'].iloc[-1]) -
                        pd.to_datetime(data['time_stamp'].iloc[0])).total_seconds())  # alt.Stunden:.seconds//3600
        self.unit = unit                                                                    # .days --> .seconds nehmen

    def __str__(self):
        return ''

    def get_mean(self):
        return self.mean

    def get_std(self):
        return self.std

    def get_skew(self):
        return self.skew

    def get_kurt(self):
        return self.kurt

    def get_rel(self):
        return self.relevance

    def get_span(self):
        return self.span

    def get_unit(self):
        return self.unit

    def set_rel(self):
        self.relevance = 1

    def set_irrel(self):
        self.relevance = 0
