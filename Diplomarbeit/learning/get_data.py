# coding: utf8
from .predict_me import *
import sqlite3
import random
import pandas as pd
from .timeseries import PyTimeBlue, PyTimeGreen, PyTimeRed


def get_data(color):

    def create_ts(color):
        path = "C:/Users/Thomas/Desktop/new_data.sqlite3"  # muss für jeden Anwendungsfall geändert werden
        # path = "/home/tilt/Classification/new_data.sqlite3" # TU - Server
        p = True

        while p is True:
            try:

                connection = sqlite3.connect(path)
                cursor = connection.cursor()
                cursor.execute("SELECT * FROM sqlite_master WHERE type='table';")  # Liste der vorhandenen Tabellennamen

                names = cursor.fetchall()  # alle Tabellennamen aus Datenbank auslesen

                # Random Nummer erzeugen - p==0 --> Sqlite_sequence, p==1 --> Senors
                # p == len(names) --> TEST
                p = random.randint(2, len(names) - 2)

                table = names[p][1]  # Name random wählen
                # print(table)

                cursor.execute("SELECT * FROM '" + table + "' ")
                cursor.fetchone()  # Test um den Error auszulösen
                p = False
                # print("Success: First try in get_data.py")
            except sqlite3.OperationalError:
                p = True

        m = 0
        p = True
        r = random.randint(1, 3)  # Drittel zufällig auswählen
        while p is True:
            if m == 10:
                if r == 1:
                    r += 1
                elif r == 2:
                    r += 1
                else:
                    r -= 1

            cursor.execute("SELECT * FROM '" + table + "' ")
            bord = (len(cursor.fetchall()))

            k = random.randint(int((r - 1) * bord / 3), int(r * bord / 3))
            n = random.randint(int((r - 1) * bord / 3), int(r * bord / 3))

            i = k < n
            cursor.execute("SELECT * FROM '" + table + "' ")
            data = pd.DataFrame(cursor.fetchall(), columns=['time_stamp', 'values'])
            try:
                if i is True:
                    data = data[k:n]  # - zufällige Skalierung des DataFrames
                else:
                    data = data[n:k]  # - zufällige Skalierung des DataFrames

                data.dropna(axis=0, how='any', inplace=True)
                m += 1
                p = False
                if data.empty:  # Test um Error auf EmptyDataFrame auszulösen
                    p = True
                # print("Success: Second try in get_data.py")
            except (IndexError, KeyError, TypeError, AssertionError):
                p = True

        data['time_stamp'] = pd.to_datetime(data['time_stamp'])

        # ---- Unit herauslesen ----

        cursor.execute(r"SELECT S.description_text FROM sensors As S WHERE uuid = '" + table + "' ")
        info = cursor.fetchall()
        unit = info[0][0]

        cursor.execute(r"SELECT S.description_text FROM sensors As S")
        all_units = list(set(cursor.fetchall()))
        trans = {}

        for p in range(len(all_units)):
            trans[all_units[p][0]] = p

        if 'blue' in color:
            time = PyTimeBlue(data=data, unit=unit)

        elif 'green' in color:
            time = PyTimeGreen(data=data, unit=unit)

        elif 'red' in color:
            time = PyTimeRed(data=data, unit=unit)

        del data
        del names
        del bord
        del info
        cursor.close()
        connection.close()

        return time, table, k, n, trans

    ts, table, k, n, trans = create_ts(color)

    j = 0
    c = True
    while c is True:
        predict, prob_rel = predict_me(ts, trans, color)
        check = prob_rel > 0.75
        loop = j <= 6
        # print("Loop No.: " + str(j))
        if loop is True:
            if predict == 1:
                if check is True:
                    # print("Check")
                    c = False
                else:
                    ts, table, k, n, trans = create_ts(color)
                    j += 1
                    # print("Predict")
                    c = True
            else:
                ts, table, k, n, trans = create_ts(color)
                j += 1
                c = True
        else:
            # print("Loop")
            c = False

    del trans
    return ts, table, k, n
