# coding: utf8
from django.shortcuts import render, get_object_or_404
from .models import *
from django.http import HttpResponse
from .get_data import *
from .make_graph import *
import gc

# Create your views here.


def index(request):
    return render(request, 'learning/index.html')


def monitoring(request):
    gc.enable()
    gc.collect()

    if 'blue' in request.POST:
        color = 'blue'

    elif 'green' in request.POST:
        color = 'green'

    elif 'red' in request.POST:
        color = 'red'

    # print(color)

    # ---- Elmente mit Relevance = 2 löschen ----

    TimeSeriesBlue.objects.filter(relevance=2).delete()
    TimeSeriesGreen.objects.filter(relevance=2).delete()
    TimeSeriesRed.objects.filter(relevance=2).delete()

    def create_blue(info, table, k, n):
        ts = TimeSeriesBlue(relevance=info.get_rel(), minimum=info.get_min(), maximum=info.get_max(),
                            mean=info.get_mean(), std=info.get_std(), span=info.get_span(), unit=info.get_unit())
        ts.save()
        fig = FigureDataBlue(number=ts, table=table, k=k, n=n)
        fig.save()
        return ts, fig

    def create_green(info, table, k, n):
        ts = TimeSeriesGreen(relevance=info.get_rel(), minimum=info.get_min(), maximum=info.get_max(),
                             start_second=info.get_start_second(), end_second=info.get_end_second(),
                             span=info.get_span(), unit=info.get_unit())
        ts.save()
        fig = FigureDataGreen(number=ts, table=table, k=k, n=n)
        fig.save()
        return ts, fig

    def create_red(info, table, k, n):
        ts = TimeSeriesRed(relevance=info.get_rel(), mean=info.get_mean(), std=info.get_std(),
                           skew=info.get_skew(), kurt=info.get_kurt(), span=info.get_span(), unit=info.get_unit())
        ts.save()
        fig = FigureDataRed(number=ts, table=table, k=k, n=n)
        fig.save()
        return ts, fig

    p = True
    while p is True:
        try:
            print("TimeSeries No.1")
            info1, table1, k1, n1 = get_data(color)    # --> KeyError
            if 'blue' in color:
                ts1, fig1 = create_blue(info1, table1, k1, n1)
            elif 'green' in color:
                ts1, fig1 = create_green(info1, table1, k1, n1)
            elif 'red' in color:
                ts1, fig1 = create_red(info1, table1, k1, n1)
            print('Save ts1')
            p = False
        except (KeyError, IndexError):
            p = True

    p = True
    while p is True:
        try:
            print("TimeSeries No.2")
            info2, table2, k2, n2 = get_data(color)  # --> KeyError
            if 'blue' in color:
                ts2, fig2 = create_blue(info2, table2, k2, n2)
            elif 'green' in color:
                ts2, fig2 = create_green(info2, table2, k2, n2)
            elif 'red' in color:
                ts2, fig2 = create_red(info2, table2, k2, n2)
            print('Save ts2')
            p = False
        except (KeyError, IndexError):
            p = True

    p = True
    while p is True:
        try:
            print("TimeSeries No.3")
            info3, table3, k3, n3 = get_data(color)  # --> KeyError
            if 'blue' in color:
                ts3, fig3 = create_blue(info3, table3, k3, n3)
            elif 'green' in color:
                ts3, fig3 = create_green(info3, table3, k3, n3)
            elif 'red' in color:
                ts3, fig3 = create_red(info3, table3, k3, n3)
            print('Save ts3')
            p = False
        except (KeyError, IndexError):
            p = True

    make_graph(ts1, fig1, ts2, fig2, ts3, fig3, color)

    time_series = {
        'ts1': ts1,
        'ts2': ts2,
        'ts3': ts3,
        'color': color,
    }

    if 'blue' in color:
        html = 'learning/monitoring_blue.html'
    elif 'green' in color:
        html = 'learning/monitoring_green.html'
    elif 'red' in color:
        html = 'learning/monitoring_red.html'

    return render(request, html, time_series)


def detail(request):
    last_input = TimeSeriesBlue.objects.order_by('-val_date')[:25]
    context = {'last_input': last_input}
    return render(request, 'learning/detail.html', context)


def relevant(request, ts_id1, ts_id2, ts_id3, color):
    if 'blue' in color:
        ts1 = get_object_or_404(TimeSeriesBlue, pk=ts_id1)
        ts2 = get_object_or_404(TimeSeriesBlue, pk=ts_id2)
        ts3 = get_object_or_404(TimeSeriesBlue, pk=ts_id3)
    elif 'green' in color:
        ts1 = get_object_or_404(TimeSeriesGreen, pk=ts_id1)
        ts2 = get_object_or_404(TimeSeriesGreen, pk=ts_id2)
        ts3 = get_object_or_404(TimeSeriesGreen, pk=ts_id3)
    elif 'red' in color:
        ts1 = get_object_or_404(TimeSeriesRed, pk=ts_id1)
        ts2 = get_object_or_404(TimeSeriesRed, pk=ts_id2)
        ts3 = get_object_or_404(TimeSeriesRed, pk=ts_id3)

    print(color)

    if "btn1r" in request.POST:
        ts1.relevance = 1
        ts1.save()

    elif "btn2r" in request.POST:
        ts2.relevance = 1
        ts2.save()

    elif "btn3r" in request.POST:
        ts3.relevance = 1
        ts3.save()

    time_series = {
        'ts1': ts1,
        'ts2': ts2,
        'ts3': ts3,
        'color': color,
    }

    if 'blue' in color:
        html = 'learning/relevant_blue.html'
    elif 'green' in color:
        html = 'learning/relevant_green.html'
    elif 'red' in color:
        html = 'learning/relevant_red.html'

    return render(request, html, time_series)


def irrelevant(request, ts_id1, ts_id2, ts_id3, color):
    if 'blue' in color:
        ts1 = get_object_or_404(TimeSeriesBlue, pk=ts_id1)
        ts2 = get_object_or_404(TimeSeriesBlue, pk=ts_id2)
        ts3 = get_object_or_404(TimeSeriesBlue, pk=ts_id3)
    elif 'green' in color:
        ts1 = get_object_or_404(TimeSeriesGreen, pk=ts_id1)
        ts2 = get_object_or_404(TimeSeriesGreen, pk=ts_id2)
        ts3 = get_object_or_404(TimeSeriesGreen, pk=ts_id3)
    elif 'red' in color:
        ts1 = get_object_or_404(TimeSeriesRed, pk=ts_id1)
        ts2 = get_object_or_404(TimeSeriesRed, pk=ts_id2)
        ts3 = get_object_or_404(TimeSeriesRed, pk=ts_id3)

    print(color)

    if "btn1i" in request.POST:
        ts1.relevance = 0
        ts1.save()

    elif "btn2i" in request.POST:
        ts2.relevance = 0
        ts2.save()

    elif "btn3i" in request.POST:
        ts3.relevance = 0
        ts3.save()

    time_series = {
        'ts1': ts1,
        'ts2': ts2,
        'ts3': ts3,
        'color': color,
    }

    if 'blue' in color:
        html = 'learning/relevant_blue.html'
    elif 'green' in color:
        html = 'learning/relevant_green.html'
    elif 'red' in color:
        html = 'learning/relevant_red.html'

    return render(request, html, time_series)


def figure(request, color):

    if 'blue' in color:
        link1 = "C:/Users/Thomas/Desktop/Monitoring/Diplomarbeit/learning/figure_blue.png"
        # link1 = "/home/tilt/Classification/Diplomarbeit/learning/figure_blue.png"
    elif 'green' in color:
        link1 = "C:/Users/Thomas/Desktop/Monitoring/Diplomarbeit/learning/figure_green.png"
        # link1 = "/home/tilt/Classification/Diplomarbeit/learning/figure_green.png"
    elif 'red' in color:
        link1 = "C:/Users/Thomas/Desktop/Monitoring/Diplomarbeit/learning/figure_red.png"
        # link1 = "/home/tilt/Classification/Diplomarbeit/learning/figure_red.png"

    image_data = open(link1, "rb").read()
    return HttpResponse(image_data, content_type="image/png")


def main(request):
    link1 = "C:/Users/Thomas/Dropbox/Diplomarbeit/PythonProgramm/Django/Diplomarbeit/learning/main.png"
    # link1 = "/home/tilt/Classification/Diplomarbeit/learning/main.png"
    image_data = open(link1, "rb").read()
    return HttpResponse(image_data, content_type="image/png")
