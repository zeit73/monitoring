# coding: utf8
import sqlite3
import pandas as pd
import numpy as np
import matplotlib.dates as mat_dates
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def make_graph(ts1, fd1, ts2, fd2, ts3, fd3, color):

    path = "C:/Users/Thomas/Desktop/new_data.sqlite3"  # muss für jeden Anwendungsfall geändert werden
    # path = "/home/tilt/Classification/new_data.sqlite3" # TU - Server
    connection = sqlite3.connect(path)
    cursor = connection.cursor()

    def make_df(fd):

        i = fd.k < fd.n
        cursor.execute("SELECT * FROM '" + fd.table + "' ")
        data = pd.DataFrame(cursor.fetchall(), columns=['time_stamp', 'values'])

        if i is True:
            data = data[fd.k:fd.n]
        else:
            data = data[fd.n:fd.k]
        data.dropna(axis=0, how='any', inplace=True)
        data['time_stamp'] = pd.to_datetime(data['time_stamp'])

        return data

    data1 = make_df(fd1)
    data2 = make_df(fd2)
    data3 = make_df(fd3)

    # ---- Plot erstellen ----

    plt.rcParams['toolbar'] = 'None'

    fig, axes = plt.subplots(ncols=3)
    fig.set_figwidth(15)
    plt.tight_layout(pad=5.5, w_pad=10)

    # ---- Farbe auswählen ----

    if 'blue' in color:
        # color = 'b'
        place = "learning/figure_blue.png"
    elif 'green' in color:
        # color = 'g'
        place = "learning/figure_green.png"
    elif 'red' in color:
        # color = 'r'
        place = "learning/figure_red.png"

    color = 'xkcd:teal'

    # print(color)

    ax = axes[0]
    ax.plot(np.array(data1['time_stamp']), np.array(data1['values']), '-', color=color)
    ax.grid()
    ax.set_title("Figure " + str(ts1.id) + "\nSensor: " + str(ts1.unit), fontsize=11)
    # if ts1.span <= 146:
    #     x_fmt = mat_dates.DateFormatter('%d %b %Y %H:%M')
    # else:
    #   x_fmt = mat_dates.DateFormatter('%d %b %Y')
    x_fmt = mat_dates.DateFormatter('%H:%M:%S.%f')
    ax.xaxis.set_major_formatter(x_fmt)
    plt.gcf().autofmt_xdate()

    ax = axes[1]
    ax.plot(np.array(data2['time_stamp']), np.array(data2['values']), '-', color=color)
    ax.grid()
    ax.set_title("Figure " + str(ts2.id) + "\nSensor: " + str(ts2.unit), fontsize=11)
    # if ts2.span <= 146:
    #     x_fmt = mat_dates.DateFormatter('%d %b %Y %H:%M')
    # else:
    #   x_fmt = mat_dates.DateFormatter('%d %b %Y')
    x_fmt = mat_dates.DateFormatter('%H:%M:%S.%f')
    ax.xaxis.set_major_formatter(x_fmt)
    plt.gcf().autofmt_xdate()

    ax = axes[2]
    ax.plot(np.array(data3['time_stamp']), np.array(data3['values']), '-', color=color)
    ax.grid()
    ax.set_title("Figure " + str(ts3.id) + "\nSensor: " + str(ts3.unit), fontsize=11)
    # if ts3.span <= 146:
    #     x_fmt = mat_dates.DateFormatter('%d %b %Y %H:%M')
    # else:
    #     x_fmt = mat_dates.DateFormatter('%d %b %Y')
    x_fmt = mat_dates.DateFormatter('%H:%M:%S.%f')
    ax.xaxis.set_major_formatter(x_fmt)
    plt.gcf().autofmt_xdate()

    fig.savefig(place)
