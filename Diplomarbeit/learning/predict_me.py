# coding: utf8
import sqlite3
import pandas as pd
import numpy as np
from sklearn import tree
import warnings
import math


def predict_me(ts, trans, color):
    warnings.simplefilter("ignore")

    path = "C:/Users/Thomas/Desktop/Monitoring/Diplomarbeit/db.sqlite3"  # muss für jeden Anwendungsfall geaendert werden
    # path = "/home/tilt/Classification/Diplomarbeit/db.sqlite3" # TU - Server

    connection = sqlite3.connect(path)
    cursor = connection.cursor()

    if 'blue' in color:
        cursor.execute("SELECT * FROM learning_timeseriesblue")
    elif 'green' in color:
        cursor.execute("SELECT * FROM learning_timeseriesgreen")
    elif 'red' in color:
        cursor.execute("SELECT * FROM learning_timeseriesred")

    info = cursor.description
    col_names = []

    for name in info:
        col_names.append(name[0])

    df = pd.DataFrame(cursor.fetchall(), columns=col_names)

    temp = df.shape[0] < 3
    # print("Temp = " + str(temp))

    if temp is True:
        predict = 1
        prob_rel = 0.99

        return predict, prob_rel

    else:
        # ---- Machine Learning Code ----

        df.drop(['id', 'val_date'], 1, inplace=True)
        df.convert_objects(convert_numeric=True)
        df['relevance'] = df['relevance'].astype(np.int64)
        df.fillna(0, inplace=True)

        def handle_non_numerical_data(frame, trans):
            columns = frame.columns.values

            for column in columns:

                def convert_to_int(val):
                    return trans[val]

                if frame[column].dtype != np.int64 and frame[column].dtype != np.float64:
                    frame[column] = list(map(convert_to_int, frame[column]))

            return frame

        df = handle_non_numerical_data(df, trans)
        # print(df.head())
        x = np.array(df.drop(['relevance'], 1))
        y = np.array(df['relevance'])

        clf = tree.DecisionTreeClassifier()
        clf.fit(X=x, y=y)

        def ts_to_array(ts):
            array = []
            for attr, value in vars(ts).items():
                array.append(value)
            return array

        def translate_unit(ts, trans):
            for h, obj in enumerate(ts):
                # print(obj)
                # print(type(obj))
                if type(obj) == str:  # alternativ: TU - Server: type(obj) == unicode
                    ts[h] = trans[obj]
                elif math.isnan(obj) is True:
                    print("NaN occured due to math.isnan() is " + str(math.isnan(obj)))
                    ts[h] = 0
                else:
                    continue

        # print(trans)
        ts = ts_to_array(ts)
        translate_unit(ts, trans)
        # print(ts)
        ts = np.delete(np.array(ts), [0])  # relevance aus dem array loeschen
        ts = [ts]
        # print(ts)
        # print(clf.classes_)
        try:
            prob_rel = float(clf.predict_proba(ts)[0][1])
        except IndexError:
            prob_rel = float(clf.predict_proba(ts)[0][0])
        predict = int(clf.predict(ts))
        # print(predict)

        return predict, prob_rel
