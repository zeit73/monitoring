from django.db import models
from django.utils import timezone
# Create your models here.


class TimeSeriesBlue(models.Model):
    relevance = models.IntegerField(default=2)
    val_date = models.DateTimeField(auto_now_add=True)
    minimum = models.FloatField(max_length=10, default=0)
    maximum = models.FloatField(max_length=10, default=0)
    mean = models.FloatField(max_length=10, default=0)
    std = models.FloatField(max_length=20, default=0)
    span = models.FloatField(max_length=15, default=0)
    unit = models.CharField(max_length=10, default='-')


class TimeSeriesGreen(models.Model):
    val_date = models.DateTimeField(auto_now_add=True)
    relevance = models.IntegerField(default=2)
    minimum = models.FloatField(max_length=10, default=0)
    maximum = models.FloatField(max_length=10, default=0)
    start_second = models.IntegerField(default=0)
    end_second = models.IntegerField(default=0)
    span = models.FloatField(max_length=15, default=0)
    unit = models.CharField(max_length=10, default='-')


class TimeSeriesRed(models.Model):
    val_date = models.DateTimeField(auto_now_add=True)
    relevance = models.IntegerField(default=2)
    mean = models.FloatField(max_length=20, default=0, null=True)
    std = models.FloatField(max_length=20, default=0, null=True)
    skew = models.FloatField(max_length=20, default=0, null=True)
    kurt = models.FloatField(max_length=20, default=0, null=True)
    span = models.FloatField(max_length=15, default=0)
    unit = models.CharField(max_length=10, default='-')


class FigureDataBlue(models.Model):
    number = models.ForeignKey(TimeSeriesBlue, on_delete=models.CASCADE)
    table = models.CharField(max_length=30, default='-')
    k = models.FloatField(max_length=10, default=0)
    n = models.FloatField(max_length=10, default=0)
    val_date = models.DateTimeField(default=timezone.now)


class FigureDataGreen(models.Model):
    number = models.ForeignKey(TimeSeriesGreen, on_delete=models.CASCADE)
    table = models.CharField(max_length=30, default='-')
    k = models.FloatField(max_length=10, default=0)
    n = models.FloatField(max_length=10, default=0)
    val_date = models.DateTimeField(default=timezone.now)


class FigureDataRed(models.Model):
    number = models.ForeignKey(TimeSeriesRed, on_delete=models.CASCADE)
    table = models.CharField(max_length=30, default='-')
    k = models.FloatField(max_length=10, default=0)
    n = models.FloatField(max_length=10, default=0)
    val_date = models.DateTimeField(default=timezone.now)



