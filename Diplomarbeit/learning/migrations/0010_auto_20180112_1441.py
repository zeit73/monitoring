# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-01-12 13:41
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('learning', '0009_auto_20180112_1356'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='timeseries',
            name='end',
        ),
        migrations.RemoveField(
            model_name='timeseries',
            name='start',
        ),
    ]
