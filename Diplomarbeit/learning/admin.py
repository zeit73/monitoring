from django.contrib import admin
from .models import TimeSeriesBlue, TimeSeriesGreen, TimeSeriesRed
# Register your models here.


admin.site.register(TimeSeriesBlue)
admin.site.register(TimeSeriesGreen)
admin.site.register(TimeSeriesRed)
